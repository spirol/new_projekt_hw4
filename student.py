import csv

with open('hw.csv', 'r', encoding='UTF-8') as h:
    file_students = csv.reader(h)
    height = 0
    weight = 0
    sd_height = 0
    sd_weight = 0

    next(file_students)
    for index, row in enumerate(file_students, start=0):
        height += float(row[1])
        weight += float(row[2])

    average_height = height / index
    average_weight = weight / index

with open('hw.csv', 'r', encoding='UTF-8') as h:

    for line in h:
        sd_height += (float(row[1]) - average_height) ** 2
        sd_weight += (float(row[1]) - average_weight) ** 2

    rms_height = (sd_height / index) ** 0.5
    rms_weight = (sd_weight / index) ** 0.5

    print(f'средний рост студентов: {average_height}')
    print(f'средний вес студентов: {average_weight}')
    print(f'cредне квадратическое отклонение роста: {rms_height}')
    print(f'cредне квадратическое отклонение веса: {rms_weight}')